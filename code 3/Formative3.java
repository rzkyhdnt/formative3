public class Formative3 {
	
	public static void main(String[] args){
		int sum = 0;

		for(int i = 0; i < args.length; i++){
			try {
				sum += Integer.parseInt(args[i]);
			} catch(NumberFormatException e){
				System.out.println("(IGNORED) Value " + args[i] + " bukan numerik. " + e.getMessage());
			}
		}	

		System.out.println("Hasil : " + sum);
		
	}
}